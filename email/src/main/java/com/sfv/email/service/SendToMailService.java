package com.sfv.email.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.security.SecureRandom;

@Service
public class SendToMailService {
    @Autowired
    private JavaMailSender javaMailSender;
    public void sendToService(String email) {
        String otp = generate();
        try{
            sendOtpToMail(email,otp);
        }catch (MessagingException e){
            throw new RuntimeException("unable to send otp");
        }
    }

    private void sendOtpToMail(String email, String otp) throws MessagingException{
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setSubject("OTP");
        mimeMessageHelper.setText(otp);
        javaMailSender.send(mimeMessage);
    }

    private String generate(){
        SecureRandom random = new SecureRandom();
        int otp = 100000 + random.nextInt(90000);
        return String.valueOf(otp);
    }
}
