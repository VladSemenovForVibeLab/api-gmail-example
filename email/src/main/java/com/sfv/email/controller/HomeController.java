package com.sfv.email.controller;

import com.sfv.email.service.SendToMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class HomeController {
    @Autowired
    private SendToMailService sendToMailService;
    private String email;
    @GetMapping("/")
    public String home(){
        return "Welcome to send otp to mail spring boot project";
    }
    @PostMapping("/send/{email}")
    public String sendToMail(@PathVariable("email") String email){
        sendToMailService.sendToService(email);
        return "send successfully";
    }
}
